# generator-pwa-starter-kit [![NPM version][npm-image]][npm-url] [![Build Status][travis-image]][travis-url] [![Dependency Status][daviddm-image]][daviddm-url]
> 

## Installation

First, install [Yeoman](http://yeoman.io) and generator-pwa-starter-kit using [npm](https://www.npmjs.com/) (we assume you have pre-installed [node.js](https://nodejs.org/)).

```bash
npm install -g yo
npm install -g generator-pwa-starter-kit
```

Then generate your new project:

```bash
yo pwa-starter-kit
```

## Getting To Know Yeoman

 * Yeoman has a heart of gold.
 * Yeoman is a person with feelings and opinions, but is very easy to work with.
 * Yeoman can be too opinionated at times but is easily convinced not to be.
 * Feel free to [learn more about Yeoman](http://yeoman.io/).

## License

MIT © [Sergo]()


[npm-image]: https://badge.fury.io/js/generator-pwa-starter-kit.svg
[npm-url]: https://npmjs.org/package/generator-pwa-starter-kit
[travis-image]: https://travis-ci.org/rogaldh/generator-pwa-starter-kit.svg?branch=master
[travis-url]: https://travis-ci.org/rogaldh/generator-pwa-starter-kit
[daviddm-image]: https://david-dm.org/rogaldh/generator-pwa-starter-kit.svg?theme=shields.io
[daviddm-url]: https://david-dm.org/rogaldh/generator-pwa-starter-kit
