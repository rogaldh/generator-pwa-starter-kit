'use strict';
const { join } = require('path');
const promisify = require('util.promisify');
const Generator = require('yeoman-generator');
const Remote = require('yeoman-remote');
const chalk = require('chalk');
const yosay = require('yosay');

const remote = promisify(Remote);

module.exports = class extends Generator {
  prompting() {
    // Have Yeoman greet the user.
    this.log(
      yosay(`Welcome to the ${chalk.red('generator-pwa-starter-kit')} generator!`)
    );

    const prompts = [
      {
        type: 'select',
        name: 'branch',
        message:
          '`pwa-starter-kit` will be installed.\nWhich branch you choose? (template-minimal-ui)',
        default: 'template-minimal-ui'
      }
    ];

    return this.prompt(prompts).then(props => {
      // To access props later use this.props.someAnswer;
      this.props = props;
    });
  }

  writing() {}

  install() {
    const repo = `Polymer/pwa-starter-kit#${this.props.branch}`;

    remote(...repo.split(/[/#]/))
      .then(path => {
        this.sourceRoot(join(path));

        return this.fs.copy(this.templatePath('**/*'), this.destinationPath(''), {
          globOptions: {
            dot: true
          }
        });
      })
      .then(() =>
        this.installDependencies({
          npm: true,
          bower: false
        })
      )
      .catch(error => {
        throw new Error(chalk.red(error));
      });
  }
};
